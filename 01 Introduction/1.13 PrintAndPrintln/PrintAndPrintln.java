
/**
  A simple program that shows how `print` and `println` work.
*/
public class PrintAndPrintln {
  public static void main(String[] args) {
    System.out.print(3);
    System.out.println(4 + 5);
  }
}
