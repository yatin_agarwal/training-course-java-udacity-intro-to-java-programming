/**
  A simple program to test if a method call access mutates the calling
  object or generates a new one.
*/
public class AccessorOrMutator2
{
  public static void main(String[] args) {
    Rectangle box = new Rectangle(5, 10, 60, 90);

    System.out.println(box.getX());
    System.out.println(box.getWidth());
    box.translate(25, 40);
    System.out.println(box.getX());
    System.out.println(box.getWidth());
  }
}
